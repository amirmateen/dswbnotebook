import os
import csv 
import MySQLdb
from subprocess import call
import pandas as pd
import numpy as np
import config

# Connect to Mysql to query table 

#connect = MySQLdb.connect(host="10.10.110.22", port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
connect=config.curr_mysql_connection
cursor = connect.cursor()

cmd = "select l.destinationuserid , coalesce(h.isupload,0), l.rarity_escalated_privileges, l.eventdate from logon_aggregation l LEFT OUTER JOIN http_aggregation h on (l.destinationuserid = h.sourceuserid and l.eventdate = h.eventdate) LEFT OUTER JOIN email_aggregation e on (l.destinationuserid = e.userid and l.eventdate = e.eventdate)"
print cmd
cursor.execute(cmd) 
result=cursor.fetchall()
connect.commit()
connect.close()

c = csv.writer(open("/root/airflow/dags/PythonFiles/ITDFILES/data_knn.csv", "wb"))

for row in result:
    c.writerow(row) 
