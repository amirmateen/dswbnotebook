import os
import csv 
import MySQLdb
from subprocess import call
import pandas as pd
import numpy as np
import config
from numpy import *

#Read in input file
df = pd.read_csv("/root/airflow/dags/PythonFiles/ITDFILES/finalx.csv", header=0)
df=df.fillna(0)
npMatrix = np.array(df)

#connect = MySQLdb.connect(host="10.10.110.22", port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
connect=config.curr_mysql_connection
cursor = connect.cursor()
#X = np.array(npMatrix)

for i in range(len(npMatrix)):
    cmd = "update logon_aggregation set unsupervised_ml_score = %s where destinationuserid = '%s' and eventdate = '%s'" % (npMatrix[i,2], npMatrix[i,0], npMatrix[i,1]) 
    #print cmd
    cursor.execute(cmd) 
    connect.commit()
connect.close()
