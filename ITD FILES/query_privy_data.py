from pyhive import hive
import os
import csv
from pyhive import hive
from subprocess import call
import pandas as pd
import numpy as np
import config

yd=(config.yyear())+"-"+(config.mmonth())+"-"+(config.dday()) 

# Connect to Hive
connect = hive.Connection(host="10.10.110.244", port=10000, username="root")
#

cursor = connect.cursor()
cmd = "select sourceuserid,from_unixtime(unix_timestamp()-1*60*60*24, '"+yd+"'), IF( count(*) = 1, 1/2 , 1/sqrt(count(*)) ) from demo.useofprivilegecommand_windowsnxlog  where months_between(timeofevent, current_timestamp()) < 4 group by sourceuserid"

print cmd
cursor.execute(cmd)
result=cursor.fetchall()
connect.commit()
connect.close()

c = csv.writer(open("/root/airflow/dags/PythonFiles/ITDFILES/eprivy_data.csv", "wb"))

for row in result:
    c.writerow(row)
