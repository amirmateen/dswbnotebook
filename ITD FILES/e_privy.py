#Fetch e_privy data
import config

#Set the date for which the commands ran
yd=(config.yester())

#demo_hive_connection=hive.Connection(host='10.10.110.244',port=10000,username='hive')
demo_hive_connection=config.curr_hive_connection.cursor()

qr="INSERT OVERWRITE LOCAL DIRECTORY '/root/airflow/dags/PythonFiles/ITDFILES/e_privy.csv' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' select sourceuserid,from_unixtime(unix_timestamp()-1*60*60*24, '"+yd+"'), IF( count(*) = 1, 1/2 , 1/sqrt(count(*)) ) from demo.useofprivilegecommand_windowsnxlog where months_between(timeofevent, current_timestamp()) < 4 group by sourceuserid"

demo_hive_connection.execute(qr)
demo_hive_connection.close()