import math
import pickle
import sys, getopt
import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.cross_validation import train_test_split
from numpy import loadtxt, where
from numpy import *
import os

#Read in command line arguments

myopts, args = getopt.getopt(sys.argv[1:],"i:o:")

ifile='data_lr'
ofile=''

###############################
# o == option
# a == argument passed to the o
###############################
for o, a in myopts:
    if o == '-i':
        ifile=a
    elif o == '-o':
        ofile=a
    else:
        print("Usage: %s -i input -o output" % sys.argv[0])

# scale larger positive and values to between -1,1 depending on the largest
# value in the data
min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-1,1))
df = pd.read_csv(ifile, header=0)

# clean up data
df.columns = ["l.destinationuserid","l.eventdate", "l.avgafterhours", "l.maxafterhours", "l.minafterhours", "l.totalafterhours","l.stdevafterhours","l.numberoftimessharedpcused","l.logon_probability",  "l.totalsharedpcusage" ,"l.year",  "l.month", "l.day",  "l.avg_after_hours", "l.max_after_hours", "l.min_after_hours","l.total_after_hours","l.stdev_after_hours", "l.number_of_times_sharedpc_used", "l.total_sharedpc_usage", "l.isthreat", "h.eventdate", "h.sourceuserid",  "h.year" , "h.month", "h.day",   "h.is_upload", "h.is_download",   "h.url_contains_job", "h.url_contains_linkedin", "h.url_contains_lockheed", "h.url_contains_monster", "h.url_contains_hire","h.url_contains_dropbox", "h.is_url_jobsearch",     "h.isthreat",  "e.userid" ,  "e.eventdate",     "e.year",  "e.month", "e.day",   "e.mean_mail_size",   "e.max_mail_size", "e.min_mail_size",     "e.mail_std_dev",  "e.total_mail_size", "e.tot_ext_dom_sent_mails",        "e.tot_ext_dom_received_mails", "e.no_of_receivers",       "e.isthreat" , "rarity_critical_asset_access"  , "rarity_escalated_privilege", "label"]

df2 =  df[["l.destinationuserid","l.eventdate"]]
numpyMatrix = np.array(df2)

x = df["label"].map(lambda x: float(x.rstrip(';')))

# formats the input data into two arrays, one of independant variables
# and one of the dependant variable
X = df[["l.avgafterhours", "l.maxafterhours", "l.minafterhours", "l.totalafterhours","l.stdevafterhours","l.numberoftimessharedpcused","l.logon_probability",  "l.totalsharedpcusage" ,"l.year",  "l.month", "l.day",  "l.avg_after_hours","l.max_after_hours", "l.min_after_hours","l.total_after_hours","l.stdev_after_hours", "l.number_of_times_sharedpc_used", "l.total_sharedpc_usage", "l.isthreat", "h.year" , "h.month", "h.day",   "h.is_upload", "h.is_download",   "h.url_contains_job", "h.url_contains_linkedin", "h.url_contains_lockheed", "h.url_contains_monster", "h.url_contains_hire","h.url_contains_dropbox", "h.is_url_jobsearch",     "h.isthreat",   "e.mean_mail_size",   "e.max_mail_size", "e.min_mail_size",     "e.mail_std_dev",  "e.total_mail_size", "e.tot_ext_dom_sent_mails",        "e.tot_ext_dom_received_mails", "e.no_of_receivers",       "e.isthreat" ,     "rarity_critical_asset_access"  ,   "rarity_escalated_privilege" ]]

#Code added from here
X = np.array(X)
where_are_NaNs = isnan(X)
X[where_are_NaNs] = 0
#till here

X = min_max_scaler.fit_transform(X)
Y = df["label"].map(lambda x: float(x.rstrip(';')))
Y = np.array(Y)


# creating testing and training set
X_test = X
Y_test = Y

#load model
pkl_filename = "/root/airflow/dags/PythonFiles/ITDFILES/pickle_model.pkl"  

with open(pkl_filename, 'rb') as file:  
    pickle_model = pickle.load(file)

score = pickle_model.score(X_test, Y_test)  
print(score)
Ypredict = pickle_model.predict_proba(X_test)  
y_predict = pickle_model.predict(X_test)
print 'Probababilities for each class from 0 to 1 {0}'.format(Ypredict)
thefile = open(ofile, 'w')


for i in range(len(Ypredict)):
    thefile.write("%s," % numpyMatrix[i,0])
    thefile.write("%s," % numpyMatrix[i,1])
    calc = Ypredict[i,1] * 100 
    thefile.write("%d\n" % calc) 

#y_predict = np.array(y_predict)

#print y_predict

f1 = f1_score(Y_test, y_predict.astype(int), 'micro')
print("F1 score: {0:8.6f} %".format(100 * f1))

pos = where(Y == 1)
neg = where(Y == 0)

##The sigmoid function adjusts the cost function hypotheses to adjust the algorithm proportionally for worse estimations
def Sigmoid(z):
	G_of_Z = float(1.0 / float((1.0 + math.exp(-1.0*z))))
	return G_of_Z 

##The hypothesis is the linear combination of all the known factors x[i] and their current estimated coefficients theta[i] 
##This hypothesis will be used to calculate each instance of the Cost Function
def Hypothesis(theta, x):
	z = 0
	for i in xrange(len(theta)):
		z += x[i]*theta[i]
	return Sigmoid(z)

##For each member of the dataset, the result (Y) determines which variation of the cost function is used
##The Y = 0 cost function punishes high probability estimations, and the Y = 1 it punishes low scores
##The "punishment" makes the change in the gradient of ThetaCurrent - Average(CostFunction(Dataset)) greater
def Cost_Function(X,Y,theta,m):
	sumOfErrors = 0
	for i in xrange(m):
		xi = X[i]
		hi = Hypothesis(theta,xi)
		if Y[i] == 1:
			error = Y[i] * math.log(hi)
		elif Y[i] == 0:
			error = (1-Y[i]) * math.log(1-hi)
		sumOfErrors += error
	const = -1/m
	J = const * sumOfErrors
	print 'cost is ', J 
	return J

##This function creates the gradient component for each Theta value 
##The gradient is the partial derivative by Theta of the current value of theta minus 
##a "learning speed factor aplha" times the average of all the cost functions for that theta
##For each Theta there is a cost function calculated for each member of the dataset
def Cost_Function_Derivative(X,Y,theta,j,m,alpha):
	sumErrors = 0
	for i in xrange(m):
		xi = X[i]
		xij = xi[j]
		hi = Hypothesis(theta,X[i])
		error = (hi - Y[i])*xij
		sumErrors += error
	m = len(Y)
	constant = float(alpha)/float(m)
	J = constant * sumErrors
	return J

##For each theta, the partial differential 
##The gradient, or vector from the current point in Theta-space (each theta value is its own dimension) to the more accurate point, 
##is the vector with each dimensional component being the partial differential for each theta value
def Gradient_Descent(X,Y,theta,m,alpha):
	new_theta = []
	constant = alpha/m
	for j in xrange(len(theta)):
		CFDerivative = Cost_Function_Derivative(X,Y,theta,j,m,alpha)
		new_theta_value = theta[j] - CFDerivative
		new_theta.append(new_theta_value)
	return new_theta

##The high level function for the LR algorithm which, for a number of steps (num_iters) finds gradients which take 
##the Theta values (coefficients of known factors) from an estimation closer (new_theta) to their "optimum estimation" which is the
##set of values best representing the system in a linear combination model
def Logistic_Regression(X,Y,alpha,theta,num_iters):
	m = len(Y)
	for x in xrange(num_iters):
		new_theta = Gradient_Descent(X,Y,theta,m,alpha)
		theta = new_theta
		if x % 100 == 0:
			#here the cost function is used to present the final hypothesis of the model in the same form for each gradient-step iteration
			Cost_Function(X,Y,theta,m)
			print 'theta ', theta	
			print 'cost is ', Cost_Function(X,Y,theta,m)
	Declare_Winner(theta)

##This method compares the accuracy of the model generated by the scikit library with the model generated by this implementation
def Declare_Winner(theta):
	score = 0
	winner = ""
	#first scikit LR is tested for each independent var in the dataset and its prediction is compared against the dependent var
	#if the prediction is the same as the dataset measured value it counts as a point for thie scikit version of LR
	scikit_score = clf.score(X_test,Y_test)
	length = len(X_test)
	for i in xrange(length):
	 	prediction = round(Hypothesis(X_test[i],theta))
		answer = Y_test[i]
		if prediction == answer:
			score += 1
	#the same process is repeated for the implementation from this module and the scores compared to find the higher match-rate
	my_score = float(score) / float(length)
	if my_score > scikit_score:
		print 'You won!'
	print 'Your score: ', my_score
	print 'Scikits score: ', scikit_score 

# These are the initial guesses for theta as well as the learning rate of the algorithm
# A learning rate too low will not close in on the most accurate values within a reasonable number of iterations
# An alpha too high might overshoot the accurate values or cause irratic guesses
# Each iteration increases model accuracy but with diminishing returns, 
# and takes a signficicant coefficient times O(n)*|Theta|, n = dataset length
initial_theta = [0,0]
alpha = 0.1
iterations = 1000
##Logistic_Regression(X,Y,alpha,initial_theta,iterations)
