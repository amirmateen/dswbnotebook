import numpy as np
import pandas as pd
import sys
import getopt
import sklearn
import config
#import pickle

#Read in command line arguments

myopts, args = getopt.getopt(sys.argv[1:],"i:o:")


ifile='/root/airflow/PythonFiles/ITDFILES/new.csv'
ofile= '/root/airflow/dags/PythonFiles/ITDFILES/finalx.csv'


#Importing the Dataset
dataset = pd.read_csv('/root/airflow/dags/PythonFiles/ITDFILES/data_knn.csv')

npData = np.array(dataset)

Xi = dataset.iloc[:, [1, 2]].values

a = (dataset['2018-06-11'] >= '2018-01-01')

#from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
#min_max_scaler = preprocessing.MinMaxScaler(feature_range=(-2,2))
mm = MinMaxScaler()
X = mm.fit_transform(Xi)

#To Find No. Of Clusters
from sklearn.cluster import KMeans
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 20)
    kmeans.fit(X)
    wcss.append(kmeans.inertia_)
#plt.plot(range(1, 11), wcss)
#plt.title('The Elbow Method')
#plt.xlabel('Number of clusters')
#plt.ylabel('WCSS')
#plt.show()

#
kmeans = KMeans(n_clusters = 6, init = 'k-means++', random_state = 20)
y_kmeans = kmeans.fit_predict(X)


centers = kmeans.cluster_centers_

#to find euclidean distance between two data points
from scipy.spatial import distance
dist = distance.cdist(X, centers, 'euclidean')

#distance of data points from cluster centers
xtoc = np.array([min(np.sum((centers-x)**2, axis=1)) for x in X])

len(xtoc)


m = xtoc.mean() #mean
s = xtoc.std() #standard deviation
z = (xtoc-m)/s #to find z -score
sd = (1.96*s) #Is Z > 1.96*(SD) to detect outlier
        
#to detect outlier
results = [int(v > sd) for v in z]


#Combining the 
dz = pd.DataFrame(z, columns = ['Score'])
dy_kmeans = pd.DataFrame(y_kmeans, columns = ['Cluster_Id'])

all_columns = pd.concat([dz,dy_kmeans], axis = 1)
dtest = pd.DataFrame(all_columns)
#dz.abs().head()
nz = pd.DataFrame(100 * ((z-min(z))/(max(z)-min(z))))

#Final Result of Outlier with Z-Score & Cluster_ID
Final_Result =  dtest.sort_values('Score', ascending = False)
final = pd.concat([dataset, nz, dy_kmeans], axis = 1)
final = np.array(final)

thefile = open(ofile, 'w')

print final;

for i in range(len(final)):
    thefile.write("%s," % final[i,0])
    thefile.write("%s," % final[i,3])
    thefile.write("%s\n" % final[i,4])
    
#print Final_Result
#matching the names with z score
# normalizing z values from 0 to 100
#no sorting of score or cluster
