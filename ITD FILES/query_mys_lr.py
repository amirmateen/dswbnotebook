import os
import csv 
import MySQLdb
from subprocess import call
import pandas as pd
import numpy as np
import os
import config
# Connect to Mysql to query table 


connect=config.curr_mysql_connection
cursor = connect.cursor()


cmd = "select l.destinationuserid,l.eventdate, cast(coalesce(l.avgafterhours,0) as decimal(13, 3)), coalesce(l.maxafterhours,0.0), coalesce(l.minafterhours,0.0), coalesce(l.totalafterhours,0.0),cast(coalesce(l.stdevafterhours,0.0) as decimal(13,3)),coalesce(l.numberoftimessharedpcused,0.0),cast(coalesce(l.logon_probability,0.0) as decimal(13,3)), coalesce(l.totalsharedpcusage,0.0) ,l.year , l.month, l.day, cast(l.avg_after_hours as decimal(13,3)), l.max_after_hours, l.min_after_hours,l.total_after_hours,cast(l.stdev_after_hours as decimal(13,3)), l.number_of_times_sharedpc_used, l.total_sharedpc_usage, l.isthreat, coalesce(h.eventdate,'0001-01-01'), coalesce(h.sourceuserid,''), coalesce(h.year,0) , coalesce(h.month,0),coalesce(h.day,0), coalesce(h.isupload,0), coalesce(h.isdownload,0), coalesce(h.urlcontainsjob,0), coalesce(h.urlcontainslinkedin,0), coalesce(h.urlcontainslockheed,0), coalesce(h.urlcontainsmonster,0), coalesce(h.urlcontainshire,0),coalesce(h.urlcontainsdropbox,0), coalesce(h.isurljobsearch,0), coalesce(h.isthreat,0), coalesce(e.userid,'') , coalesce(e.eventdate,'0001-01-01'), coalesce(e.year,0), coalesce(e.month,0), coalesce(e.day,0), coalesce(cast(e.meanmailsize as decimal(13,3)),0.0), coalesce(e.maxmailsize,0.0), coalesce(e.minmailsize,0.0), coalesce(cast(e.mailstdev as decimal(13,3)),0.0), cast(coalesce(e.totalmailsize,0.0) as decimal(13,3)), coalesce(e.tot_ext_dom_sent_mails,0.0), coalesce(e.tot_ext_dom_received_mails,0.0), coalesce(e.noofreceivers,0),  coalesce(e.isthreat,0.0) , coalesce(l.rarity_critical_asset_access, 0.0), coalesce(l.rarity_escalated_privileges,0.0),'0;;;;' as label from logon_aggregation l LEFT OUTER JOIN http_aggregation h on (l.destinationuserid = h.sourceuserid and l.eventdate = h.eventdate) LEFT OUTER JOIN email_aggregation e on (l.destinationuserid = e.userid and l.eventdate = e.eventdate)"
print(cmd)
cursor.execute(cmd)
result=cursor.fetchall()
connect.commit()
connect.close()

c = csv.writer(open("/root/airflow/dags/PythonFiles/ITDFILES/data_lr.csv", "wb"))

for row in result:
    c.writerow(row)
##Dbconfig  
# Demo_mysql_connection  -10.10.110.244 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
# Demo_hive_connection -10.10.110.244 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
# Demo_presto_connection -10.10.110.244 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
# prod_mysql_connection - 10.10.110.22 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
# prod_hive_connection -10.10.110.22 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
# prod_presto_connection -10.10.110.22 port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')