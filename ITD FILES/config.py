from datetime import datetime, timedelta, date
import MySQLdb
from pyhive import hive

demo_mysql_connection=MySQLdb.connect(host='10.10.110.244', port=3306, user='security_lytics', passwd='security_lytics$123', db='security_analytics', charset='utf8')

demo_hive_connection=hive.Connection(host='10.10.110.244',port=10000,username='hive')


curr_mysql_connection=demo_mysql_connection
curr_hive_connection=demo_hive_connection

#Current Hive connection in use
curr_hive_connection=demo_hive_connection

# Setting up the date for which the commands will run
def yester():
    executeForDate = date.today()-timedelta(1)
    efd='20'+str(executeForDate.strftime('%y%m%d'))
    return efd;

# Setting up the year, Month and Date for the queries
def yyear():
    executeForDate = date.today()-timedelta(1)
    efd='20'+str(executeForDate.strftime('%y'))
    return efd;
def mmonth():
    executeForDate = date.today()-timedelta(1)
    efd=str(executeForDate.strftime('%m'))
    return efd;
def dday():
    executeForDate = date.today()-timedelta(1)
    efd=str(executeForDate.strftime('%d'))
    return efd;
    