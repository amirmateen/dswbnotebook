#
#
#

import MySQLdb
import pandas as pd
import numpy as np
import sys, getopt
from pyhive import hive
import datetime
import config
print(datetime.datetime.now())

fromdate = ''
todate = ''

myopts, args = getopt.getopt(sys.argv[1:],"f:t:")
for o, a in myopts:
    if o == '-f':
        fromdate=a
    elif o == '-t':
        todate=a
    else:
        print("Usage: %s -f fromdate -t todate" % sys.argv[0])

try:
    if fromdate != '' and todate == '':
        todate = fromdate

    if fromdate != '' and todate != '':
        sql = "select hostname from printers where length(rtrim(hostname)) > 0"
        #connect = MySQLdb.connect(host="10.10.110.22", port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
        connect=config.curr_mysql_connection
        df = pd.read_sql_query(sql,connect)
        sql = "select to_date(event_time) as eventdate,regexp_replace(regexp_replace(info_6,'src_user=',''),'@sstech.internal','') destinationuserid,dst_ip hostname ,count(dst_ip) printercnt from demo.wgtraffic where (to_date(event_time)>='" + fromdate + "' and to_date(event_time)<='" + todate + "')and length(rtrim(info_6))>0 and instr(info_6,'src_user=')>0 and dst_port in (9101, 9102, 515, 631) group by to_date(event_time),regexp_replace(regexp_replace(info_6,'src_user=',''),'@sstech.internal',''),dst_ip"
	
        #conn =  hive.Connection(host="10.10.110.77",port=10000,username="hive")
        conn=config.curr_hive_connection


        df_data = pd.read_sql_query(sql, conn)
        result = pd.merge(left=df, right=df_data, on="hostname", how="inner")
		
        df_req = pd.DataFrame(result, columns = ['destinationuserid','eventdate','printercnt'])
        df_req = df_req.groupby(["destinationuserid","eventdate"])["printercnt"].sum().reset_index(name="score")
        print "df---"+df_req
        print 'my'

        npMatrix = np.array(df_req)
        #connect = MySQLdb.connect(host="10.10.110.22", port=3306, user="security_lytics", passwd="security_lytics$123", db='security_analytics', charset='utf8')
        connect=config.curr_mysql_connection
        cursor = connect.cursor()

        for i in range(len(npMatrix)):
            cmd = "update logon_aggregation set isPrinter =  %s where destinationuserid = '%s' and eventdate = '%s'" % (npMatrix[i,2], npMatrix[i,0], npMatrix[i,1])
            print cmd
            cursor.execute(cmd)
            connect.commit()
        connect.close()
		
        print 'Success.'
    else:
        print '[usage]: update_printer_score -f fromdate -t todate'
except Exception,e:
    print '[ERROR]' , str(e)

print(datetime.datetime.now())
