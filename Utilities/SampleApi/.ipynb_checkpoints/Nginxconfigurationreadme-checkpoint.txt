In server block of nginx configuration We need to to have location block like this-


location /api5/{        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://localhost:5050/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
        client_max_body_size 20M;
        add_header 'Access-Control-Allow-Origin' "*" always;
        add_header 'Access-Control-Allow-Credentials' 'true' always;
        }

Where in this line-
        proxy_pass http://localhost:5050/;

you type port no on which flask app is running

Then restart nginx
