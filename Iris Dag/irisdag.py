# airflowRedditPysparkDag.py
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import os


## Define the DAG object
default_args = {
    'owner': 'sstech',
    'depends_on_past': False,
    'start_date': datetime(2016, 10, 15),
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}
dag = DAG('irisclassdag', default_args=default_args, schedule_interval=timedelta(1))

fetchdata = BashOperator(
    task_id='Fetch_Data',
    bash_command='python3 load_iris.py',
    dag=dag
)
savedata = BashOperator(
    task_id='save_data',
    bash_command='python3 save_data.py',
    dag=dag
)
savedata.set_upstream(fetchdata)






