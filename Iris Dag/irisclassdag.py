from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime,timedelta
import os


default_args={'owner':'Author',
             'depends_on_passed':False,
             'start_date':datetime(2018, 06, 01),
             'retries':5,
             'retry_delay':timedelta(minutes=1),
             }
dag=DAG('irisclassdag',default_args=default_args, schedule_interval=timedelta(1))

fetch_data=BashOperator(
        task_id='fetch_data',
        bash_command='python3 load_iris.py',dag=dag)

save_data=BashOperator(
        task_id='save_data',
        bash_command='python3 save_data.py',dag=dag)
save_data.set_upstream(fetch_data)
    